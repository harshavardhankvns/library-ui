import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);
const vuetify = new Vuetify({
    theme: {
      themes: {
        light: {
          primary: '#004AAD',
          secondary: '#545454',
          accent: '#5271FF',
          error: '#b71c1c',
        },
      },
    },
  })
export default vuetify;
