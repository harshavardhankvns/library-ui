import Vue from 'vue'
import VueRouter from 'vue-router'
import TheBooks from '../views/TheBooks.vue'
import TheDashboard from '../views/TheDashboard.vue'
import TheLaptops from '../views/TheLaptops.vue'
import MyProfile from '../views/MyProfile.vue'
import StudentQueries from '../views/StudentQueries.vue'
import EBooks from '../views/EBooks.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'TheDashboard',
        component: TheDashboard
    },
    {
        path: '/Books',
        name: 'TheBooks',
        component: TheBooks
    },
    {
        path: '/dashboard',
        name: 'TheDashboard',
        component: TheDashboard
    },
    {
        path: '/MyProfile',
        name: 'MyProfile',
        component: MyProfile
    },
    {
        path: '/Laptops',
        name: 'TheLaptops',
        component: TheLaptops
    },
    {
        path: '/Queries',
        name: 'StudentQueries',
        component: StudentQueries
    },
    {
        path: '/EBooks',
        name: 'EBooks',
        component: EBooks
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router