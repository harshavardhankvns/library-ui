import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        isLoggedIn: { Id: null, Student: null },
        apiBaseURL: "http://example.inspiroheights.com/Library.API/",
        studentId: null,
        student: {},
        studentMembership: {},
        studentBooks: {},
        allBooks: [],
        allEBooks: [],
        BookFine: [],
        BooksFines: [],
        LaptopsFines: [],
        LaptopFine: [],
        Laptops: {},
        myLaptop: {},
    },
    getters: {
        isLogIn: (state) => {
            return state.isLoggedIn.Student;
        },
        activeStudent: (state) => {
            return state.student;
        },
        allBooks: (state) => {
            return state.allBooks;
        },
        allEBooks: (state) => {
            return state.allEBooks;
        },
        studentBooks: (state) => {
            return state.studentBooks;
        },
        Laptops: (state) => {
            return state.Laptops;
        },
        Laptop: (state) => {
            return state.myLaptop;
        },
        BookFine: (state) => {
            return state.BookFine;
        },
        LaptopFine: (state) => {
            return state.LaptopFine;
        },
        BooksFines: (state) => {
            return state.BooksFines;
        },
        LaptopsFines: (state) => {
            return state.LaptopsFines;
        },
    },
    mutations: {
        async setIsLoggedIn(state) {
            await axios
                .get(`http://localhost:8012/Library/Library.API/Student-IsLoggedIn.php`)
                .then((response) => {
                    state.isLoggedIn = response.data;
                    state.studentId = parseInt(state.isLoggedIn.Id);
                    alert(state.isLoggedIn.Id)
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getStudent(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `/Students/GetById.php?Id=` +
                    state.studentId
                )
                .then((response) => {
                    state.student = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        putStudent(state) {
            axios
                .post(state.apiBaseURL + `/Students/Put.php`, state.student)
                .then((response) => {
                    if (response.data == "Success")
                        alert("Sucessfully updated student");
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getAllBooks(state) {
            axios
                .get(state.apiBaseURL + `/Books/GetAll.php`)
                .then((response) => {
                    state.allBooks = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getAllEBooks(state) {
            axios
                .get(state.apiBaseURL + `/EBooks/GetAll.php`)
                .then((response) => {
                    state.allEBooks = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getBooksByStudentId(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `/Books/GetByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.studentBooks = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getLaptops(state) {
            axios
                .get(state.apiBaseURL + `/Laptops/GetAll.php`)
                .then((response) => {
                    state.Laptops = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getMyLaptop(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `Laptops/GetByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.myLaptop = response.data[0];
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getMyBookFine(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `BookFines/GetByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.BookFine = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getMyBooksFines(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `Books/GetFinesByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.BooksFines = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getMyLaptopFine(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `LaptopFines/GetByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.LaptopFine = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
        getMyLaptopsFines(state) {
            axios
                .get(
                    state.apiBaseURL +
                    `Laptops/GetFinesByStudentId.php?StudentId=` +
                    state.studentId
                )
                .then((response) => {
                    state.LaptopsFines = response.data;
                })
                .catch((e) => {
                    errors.push(e);
                });
        },
    },
    actions: {
        setIsLoggedIn(context) {
            context.commit("setIsLoggedIn");
        },
        getStudent(context) {
            context.commit("getStudent");
        },
        putStudent(context) {
            context.commit("putStudent");
        },
        getAllBooks(context) {
            context.commit("getAllBooks");
        },
        getAllEBooks(context) {
            context.commit("getAllEBooks");
        },
        getBooksByStudentId(context) {
            context.commit("getBooksByStudentId");
        },
        getLaptops(context) {
            context.commit("getLaptops");
        },
        getMyLaptop(context) {
            context.commit("getMyLaptop");
        },
        getMyBookFine(context) {
            context.commit("getMyBookFine");
        },
        getMyLaptopFine(context) {
            context.commit("getMyLaptopFine");
        },
        getMyBooksFines(context) {
            context.commit("getMyBooksFines");
        },
        getMyLaptopsFines(context) {
            context.commit("getMyLaptopsFines");
        },
    },
    modules: {},
});